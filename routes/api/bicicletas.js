var express = require('express');
var router = express.Router();
var BicicletaController = require ('../../controllers/api/bicicletaControllerAPI');

router.get('/',BicicletaController.bicicleta_list);
router.post('/create', BicicletaController.bicicleta_create);
router.post('/:code/delete', BicicletaController.bicicleta_delete);
router.post('/:id/update', BicicletaController.bicicleta_update_post);

module.exports = router;
