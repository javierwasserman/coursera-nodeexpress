var express = require('express');
var router = express.Router();

var UsuarioController = require ('../controllers/usuario');
const Usuario = require('../models/usuario');

router.get('/',UsuarioController.usuario_list); // ruta de la pagina. Pega contra el controller y que metodo.
router.get('/create',UsuarioController.usuario_create_get);
router.post('/create',UsuarioController.usuario_create_post);
router.post('/:id/delete',UsuarioController.usuario_delete_post);
router.get('/:id/update',UsuarioController.usuario_update_get);
router.post('/:id/update',UsuarioController.usuario_update_post);

module.exports = router;
