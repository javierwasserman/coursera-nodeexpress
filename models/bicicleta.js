var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number, 
    color: String, 
    modelo: String, 
    ubicacion:{
        type: [Number],index: {type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new this({
        code:code,color:color,modelo:modelo,ubicacion:ubicacion
    });
};

bicicletaSchema.methods.toString = () => {
    return 'id: ' + this.code + ' | Color : ' + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
}

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici,cb);
}

bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code:aCode},cb);
}

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    //console.log('Borrando ' + aCode);
    return this.deleteOne({code:aCode},cb);
}


module.exports = mongoose.model('Bicicleta',bicicletaSchema);


/*var Bicicleta = function(id,color,modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

var a = new Bicicleta(15,'Rojo','Carrera',[-34.6012424,-58.3861497]);
var b = new Bicicleta(2,'Blanca','Urbana',[-34.5962424,-58.3861497]);
var c = new Bicicleta(3,'Verde','Plegable',[-34.5952424,-58.3826497]);

//Bicicleta.add(a);
//Bicicleta.add(b);
//Bicicleta.add(c);

Bicicleta.findById = (aBiciId)=>{
    //console.log(aBiciId);
    var aBici = Bicicleta.allBicis.find(x=> x.id == aBiciId);
    if(aBici)
        return aBici;
    else    
        throw new Error('No existe tal Bicicleta con el id ' + aBiciId);
}

Bicicleta.removeById = (id) =>{
    //var aBici = Bicicleta.findById(id);
    for(var i = 0; i< Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == id){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }

}
module.exports = Bicicleta;
*/