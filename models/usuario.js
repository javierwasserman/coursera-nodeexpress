var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const Token = require('../models/token');
const mailer = require('../mailer/mailer');


const saltRounds = 10;

const validateEmail = function(email){
    
    return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email));

}
var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim:true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail,'El email es invalido'],
        match: [/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/]
    },
    password:{
        type:String, 
        required:[true,'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type:Boolean,
        default:false
    },
    googleId:String,
    facebookId:String

});

usuarioSchema.plugin(uniqueValidator,{message:'El {PATH} ya existe'});


// se ejecuta antes, en este caso, de usuarios.save
usuarioSchema.pre('save',function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password,saltRounds);    
    }
    next();

})

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password,this.password);
}
usuarioSchema.methods.reservar = function(biciId,desde,hasta,cb){
    var reserva = new Reserva({
            usuario: this._id,
            bicicleta:biciId,
            desde:desde,
            hasta:hasta
        
        })
        console.log(reserva);
        reserva.save(cb);
}

usuarioSchema.statics.allUsers = function(cb){
    return this.find({},cb);
}


usuarioSchema.statics.findByEmail = function(email, cb){
    return this.findOne({email:email},cb);
}

usuarioSchema.statics.removeByEmail = function(email, cb){
    //console.log('Borrando ' + aCode);
    return this.deleteOne({email:email},cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    console.log(token);
    const email_destination = this.email;
    token.save(function(err){
        if(err){
            return console.log(err.message);
        }
        const mailOptions = {
            from: 'otilia.boyle@ethereal.email',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en el siguiente link: \n\n' + process.env.URL + '\/token/confirmation\/' + token.token
        };

        console.log(mailOptions);

        mailer.sendMail(mailOptions, function(err){
            if(err){
                return console.log(err.message);
            }
            console.log('Se ha enviado un correo de verificacion a: ' + email_destination);
        });
    });
}


usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){ return cb(err); }

        const mailOptions = {
            from: 'no-reply@jwass.ar',
            to: email_destination,
            subject: 'Reseteo del password de tu cuenta',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en el sigueinte link:\n' + 'http://localhost:5000' + '\/resetPassword\/' + token.token
        };
        console.log(mailOptions);

        mailer.sendMail(mailOptions, function(err){
            if(err){ return cb(err); }
            console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
        });

        cb(null);
    })
}


usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    console.log('-------------- CONDITION --------------');
    console.log(condition);
    self.findOne({
        $or: [
            {'googleId': condition.profile.id}, {'email': condition.profile.emails[0].value}
        ]}, (err, result) => {
            console.log('--------------- RESULT -----------------');
            console.log(result);
            if(err) { console.log(err); }
            if(result){
                if(err) { console.log(err); }
                callback(err, result)
            }else{
                let values = {};
                values.googleId = condition.profile.id,
                values.email = condition.profile.emails[0].value,
                values.nombre = condition.profile.displayName || 'SIN NOMBRE',
                values.verificado = true,
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('-------------- VALUES -----------------');
                console.log(values);
                self.create(values, (err, result) => {
                    if(err) { console.log(err); }
                    return callback(err, result)
                })
            }
        }
    )
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    const self = this;
    console.log('-------------- CONDITION --------------');
    console.log(condition);
    //console.log(condition.email);
    self.findOne({
        $or: [
            {'facebookId': condition.profile.id}, {'email': condition.profile.emails[0].value}
        ]}, (err, result) => {
            //console.log('--------------- RESULT -----------------');
            //console.log(result);
            if(err) { console.log(err); }
            if(result){
                if(err) { console.log(err); }
                callback(err, result)
            }else{
                let values = {};
                
                values.facebookId = condition.profile.id,
                values.email = condition.profile.emails[0].value,
                values.nombre = condition.profile.displayName || 'SIN NOMBRE',
                values.verificado = true,
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('-------------- VALUES -----------------');
                console.log(values);
                self.create(values, (err, result) => {
                    if(err) { console.log('error creando usuario de fb:' + err); }
                    return callback(err, result)
                })
            }
        }
    )
}


module.exports = mongoose.model('Usuario',usuarioSchema);