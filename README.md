This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Desarollo hecho para aprender Express, node y Mongo, para el curso de  https://www.coursera.org/learn/desarrollo-lado-servidor-nodejs-express-mongodo

[https://red-bicicletas-jw.herokuapp.com/] (Link de acceso Heroku)


## Creado por

### `Javier A. Wasserman`

(https://twitter.com/javierwasserman)[Twitter]

[https://github.com/jamarks](GitHub)

[https://bitbucket.org/javierwasserman/](BitBucket)


### Ethereal 
Account credentials
Account details generated using Faker.js

NB! these credentials are shown only once. If you do not write these down then you have to create a new account.

Name	Otilia Boyle
Username	otilia.boyle@ethereal.email (also works as a real inbound email address)
Password	a754bRhTZJ4Y1U42Yt
