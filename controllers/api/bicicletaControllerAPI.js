var Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = (req,res) => {
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({
            bicicletas: bicis
        });
        
    });
}

exports.bicicleta_create = (req,res) => {
    //console.log(req.body);
    var ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng];
    var bici = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo, ubicacion);
    //bici.ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng];

    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = (req,res) => {
    console.log(req.body);
    Bicicleta.findByCode(req.body.id,function(err,bici){
        if(err) 
            res.status(404).send();
        
        //res.status(204).send();   
        
        if(bici){
            Bicicleta.removeByCode(req.body.id,function(err){
                if(err) {
                    console.log(err);
                    res.status(404).send();    
                }                
                res.status(204).send();    

            });            
        }else{
            console.log('No se encuentra la bici con id ' + req.body.id);
           res.status(404).send();    
        }
    });
}

exports.bicicleta_update_post = (req,res) => {
    console.log(req.params);
    Bicicleta.findByCode(req.params.id,function(err,bici){
        
        if(err){
            res.status(404).send()
            
        }

        if(bici){
            if(req.body.id)
                bici.code = req.body.id

            if(req.body.color)
                bici.color = req.body.color
            
            if(req.body.modelo)
                bici.modelo = req.body.modelo

            if(req.body.ubicacion)
                bici.ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng]
            
            bici.save();
            
            res.status(200).json({
                bicicleta: bici
            })
            
        }else
        {console.log('No se encuentra la bici con id ' + req.params.id);
        res.status(500).send();}
            
    });
}