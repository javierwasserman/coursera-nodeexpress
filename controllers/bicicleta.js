var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = (req,res) => {

    // funciona
    Bicicleta.allBicis(function(err,bicis){
        
        //console.log(bicis);
        res.render('bicicletas/index',{
            bicis: bicis
            
        });
        
    });


    
}

exports.bicicleta_create_get = (req,res) =>{
    // funciona
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = (req,res) =>{
    //res.render('bicicletas/create');
    // funciona
    //console.log(req.body);
    var ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng];
    var bici = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo, ubicacion);
    //bici.ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng];

    Bicicleta.add(bici);
    res.redirect('/bicicletas');

}
exports.bicicleta_delete_post = (req,res)=>{
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');

}

exports.bicicleta_update_get = (req,res) =>{
    var bici = Bicicleta.findByCode(req.params.id,function(err,bici){
        res.render('bicicletas/update',{bici});

    });
    
}

exports.bicicleta_update_post = (req,res) =>{
    //console.log(req.params);
    //console.log(req.body);

    Bicicleta.findByCode(req.params.id,function(err,bici){
        
        if(err){
            res.status(404).send()
            
        }

        if(bici){
            if(req.body.code)
                bici.code = req.body.code

            if(req.body.color)
                bici.color = req.body.color
            
            if(req.body.modelo)
                bici.modelo = req.body.modelo

            if(req.body.ubicacion)
                bici.ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng]
            
            bici.save();
            
            res.redirect('/bicicletas');
            
        }else
        {console.log('No se encuentra la bici con id ' + req.params.id);
        res.status(404).send();
        }
            
    });


}
exports.bicicleta_delete_post = (req,res)=>{
    
    Bicicleta.removeByCode(req.body.code,function(err){
        
        if(err)
            console.log(err);

        res.redirect('/bicicletas');
        
    });
    

}


