var Usuario = require('../models/usuario');

exports.usuario_list = (req,res) => {
    Usuario.allUsers(function(err,usuarios){
        
        //console.log(bicis);
        res.render('usuarios/index',{
            usuarios: usuarios
            
        });        
    });
    
}

exports.usuario_create_get = function(err, res){
    res.render('usuarios/create', {errors: {}, usuario: new Usuario()});
}
exports.usuario_create_post = function(req, res){
    
    if(req.body.password != req.body.confirm_password){
        res.render('usuarios/create', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
        return;
    }

    Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, newUsuario) {
        if(err){
            res.render('usuarios/create', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
        }else{
            console.log('envio bienvenida');
            newUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios')
        }
    });
}

exports.usuario_update_get = function(req, res){
    Usuario.findById(req.params.id,function(err,user){
        //console.log(req.params.id);
        //console.log(user);
        if(err)
            console.log(er);

        if(user)
        {
            res.render('usuarios/update', {errors: {}, usuario: user});
        }
    })
}

exports.usuario_update_post = function(req, res){
    // solo modifica el nombre.
    Usuario.findByIdAndUpdate(req.params.id, {"nombre": req.body.nombre}, function(err, Usuario){
        if(err){
            console.log(err);
            res.render('usuarios/update',  {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
        }else{
            res.redirect('/usuarios');
        }
    });
}

exports.usuario_delete_post = function(req, res, next){
    Usuario.findByIdAndDelete(req.body.id, function(err){
        if(err){
            next(err);
        }else{
            res.redirect('/usuarios');
        }
    });
}
