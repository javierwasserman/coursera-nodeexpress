var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
// /var server = require('../../bin/www');

describe('Testing Bicicletas mongoDb',function(){

    //beforeAll((done) => { mongoose.connection.close(done) });

    beforeAll(function(done){
        mongoose.disconnect();
        var mongoDb = 'mongodb+srv://jamarks:L5eDB3orxoBwjpML@cluster0.an1py.gcp.mongodb.net/bicicletas?retryWrites=true&w=majority';
        mongoose.connect(mongoDb,{useNewUrlParser:true,useUnifiedTopology: true,useCreateIndex: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',()=>{
            console.log('Conectado a la base');
            done();
        });

        
    });

    afterEach(function(done){
            Bicicleta.deleteMany({},(err,success)=>{
                if(err) console.log(err);
                done();
            });

    });

    describe('Bicicleta.createInstance',()=>{
        it('Crea instancia de Bicicleta',(done)=>{
            var bici = Bicicleta.createInstance(1,"verde moco","urbana",[-34.5,-54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde moco');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
            done();

        });
    });

    describe('Bicicleta.AllBicis',() => {
        it('Comienza Vacia',(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',()=>{
        it('Agrega solo una bici',(done)=>{
            var aBici = new Bicicleta({code:1, color:'amarillo',modelo:'ciudad'});
            Bicicleta.add(aBici,function(err,newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    
                    done();

                })
            })
        })
    })

    describe('Bicicleta.findByCode',()=>{
        it('Debe devolver la bici con code 1',(done)=>{
            Bicicleta.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:'amarillo',modelo:'ciudad'});
                Bicicleta.add(aBici,(err,newBici)=>{
                    if(err) console.log(err);
                   
                    var aBici2 = new Bicicleta({code:1, color:'amarillo',modelo:'ciudad'});
                    Bicicleta.add(aBici2,(err,newBici)=>{
                        if(err) console.log(err);
                        
                        Bicicleta.findByCode(1,(error,targetBici)=>{
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        })
                    })
                })
            })
        })
    })

    describe('Bicicleta.removeByCode',()=>{
        it('Debe borrar la bici con code 1',(done)=>{
            Bicicleta.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:'amarillo',modelo:'ciudad'});
                Bicicleta.add(aBici,(err,newBici)=>{
                    if(err) console.log(err);
                    //console.log(bi)
                   
                    var aBici2 = new Bicicleta({code:2, color:'amarillo',modelo:'ciudad'});
                    Bicicleta.add(aBici2,(err,newBici)=>{
                        if(err) console.log(err);
                        
                        //console.log(bicis.length);
                        //expect(bicis.length).toBe(2);
                        Bicicleta.removeByCode(1,(error,targetBici)=>{
                            Bicicleta.allBicis((err,bicis)=>{
                                expect(bicis.length).toBe(1);
                                done();
                            });
                        })
                    })
                })
            })
        })
    })
})