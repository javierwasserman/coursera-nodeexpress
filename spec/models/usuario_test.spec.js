var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Usuarios',function(){

    beforeAll(function(done){
        mongoose.disconnect();
        var mongoDb = 'mongodb+srv://jamarks:L5eDB3orxoBwjpML@cluster0.an1py.gcp.mongodb.net/bicicletas?retryWrites=true&w=majority';
        mongoose.connect(mongoDb,{useNewUrlParser:true,useUnifiedTopology: true,useCreateIndex: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',()=>{
            console.log('Conectado a la base para probar Usuarios');
            done();
        });

        
    });

    afterEach(function(done){

            Reserva.deleteMany({},function(err,success){
                if(err) console.log(err);

                Usuario.deleteMany({},function(err,success){
                    if(err) console.log(err);

                    Bicicleta.deleteMany({},function(){
                        if(err) console.log(err);

                        done();

                    });
                });


            });

    });

    
    describe('Usuario reserva una bici.',()=>{
        it('Debe existir la reserva',(done)=>{
            const usuario = new Usuario({nombre:'Javier'});
            usuario.save();
            
            const bicicleta = new Bicicleta({code:1,color:'Negra',modelo:'Plegable',ubicacion:[-34,-45]});
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();

            manana.setDate(hoy.getDate()+1);

            usuario.reservar(bicicleta.id,hoy,manana,function(){
              Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){

                //console.log(reservas[0]);
                expect(reservas.length).toBe(1);
                expect(reservas[0].diasDeReserva()).toBe(2);
                expect(reservas[0].bicicleta.code).toBe(1);
                expect(reservas[0].usuario.nombre).toBe(usuario.nombre);


                done();
              })  
            })



        })

    })


});
