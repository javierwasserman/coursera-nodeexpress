var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
//var server = require('../../bin/www');
var request = require('request');


var base_url = 'http://localhost:5000/api/bicicletas';


/*beforeAll(()=>{
   console.log('testeando');
})*/

describe('Bicicleta API',()=>{

    beforeAll(function(done){
        mongoose.disconnect();
        var mongoDb = 'mongodb+srv://jamarks:L5eDB3orxoBwjpML@cluster0.an1py.gcp.mongodb.net/bicicletas?retryWrites=true&w=majority';
        mongoose.connect(mongoDb,{useNewUrlParser:true,useUnifiedTopology: true,useCreateIndex: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',()=>{
            console.log('Conectado a la base');
            done();
        });

        
    });

    afterEach(function(done){
          Bicicleta.deleteMany({},(err,success)=>{
                if(err) console.log(err);
                console.log('Borrando bicis para empezar');
                done();
            });
    });

    


    describe('GET BICICLETAS /',()=>{
        it('Status 200',(done)=>{

            request.get(base_url,function(error,response,body){
                
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                //console.log(result);
                //expect(result.bicicletas.length).toBe(0);
                done();
            })
        
        })
    })

    describe('POST BICICLETAS /create',()=>{
        it('STATUS 200',(done)=>{
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":"15", "color":"Rojo", "modelo":"Verga", "ubicacionlat" : "-34", "ubicacionlng": "-54"}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                //console.log(bici);
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            })
        })
    }) 

    describe('POST bicicleta/delete',()=>{
        it('STATUS 200',(done)=>{
            
            //var ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng];
            var aBici = Bicicleta.createInstance(19, 'Amarillo Delete', 'Super Carrera', [-34.555,-58.00000]);
            Bicicleta.add(aBici,function(err,bici){


                var headers = {'content-type' : 'application/json'};
                var aBici = '{"id":"19"}';

                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/delete',
                    body: aBici
                },function(error,response,body){
                    expect(response.statusCode).toBe(204);
                    //expect(Bicicleta.allBicis.length).toBe(0);
                    done();
                 })
            })
            
            
                //expect(Bicicleta.allB).toBe(204);
                
            })
        })

        describe('POST bicicleta/update',()=>{
            it('STATUS 200',(done)=>{
                
                //var ubicacion = [req.body.ubicacionlat, req.body.ubicacionlng];
                var aBici = Bicicleta.createInstance(19, 'Amarillo Delete', 'Super Carrera', [-34.555,-58.00000]);
                Bicicleta.add(aBici,function(err,bici){
    
    
                    var headers = {'content-type' : 'application/json'};
                    var aBici = '{"color":"verde"}';
    
                    request.post({
                        headers: headers,
                        url: 'http://localhost:5000/api/bicicletas/19/update',
                        body: aBici
                    },function(error,response,body){
                        expect(response.statusCode).toBe(200);
                        var bici = JSON.parse(body).bicicleta;
                        
                        expect(bici.color).toBe('verde');
                        
                        done();
                     })
                })
            })
        })
});


